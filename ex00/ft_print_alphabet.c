#include "../../ft_putchar/ft_putchar.h"

void ft_print_alphabet(void) 
{
	char i;

	i = 'a';

	while (i <= 'z') 
	{
		ft_putchar(i);
		i++;
	}

	ft_putchar('\n');	
}