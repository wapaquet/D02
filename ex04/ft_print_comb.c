#include "../../ft_putchar/ft_putchar.h"

void printNumb(int i, int j, int k) 
{
	ft_putchar(i);
	ft_putchar(j);
	ft_putchar(k);

	if (i < '7' && j <= '8' && k <= '9')
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void ft_print_comb(void) 
{
	int c;
	int d;
	int u;
	
	c = '0';
	d = '1';
	u = '2';

	while (c <= '7' && d <= '8' && u <= '9')
	{
		printNumb(c, d, u);
		u++;
		if (u > '9') 
		{
			d++;
			u = d + 1;
		}
		if (d > '8') 
		{
			c++;
			d = c + 1;
			u = d + 1;
		}
	}
}
