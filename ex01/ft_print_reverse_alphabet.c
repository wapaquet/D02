#include "../../ft_putchar/ft_putchar.h"

void ft_print_reverse_alphabet(void)
{
	char i;

	i = 'z';

	while (i >= 'a') 
	{
		ft_putchar(i);
		i--;	
	}
	
	ft_putchar('\n');	
}